server {
    listen          80;
    server_name     bassa.com www.bassa.com;

    location / {
        proxy_pass http://bassa-api:7656;
    }

    location /lekana {
        rewrite ^/lekana(.*)$ $1 break;
        proxy_pass http://lekana-api:7654;
    }

    location /siddhi {
        rewrite ^/siddhi(.*)$ $1 break;
        proxy_pass http://siddhi-api:7655;
    }
}
