package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/bassa", apiBassa).Methods("GET")

	// start server
	err := http.ListenAndServe(":7656", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiBassa(w http.ResponseWriter, r *http.Request) {
	log.Printf("INFO: bassa api")

	fmt.Fprintln(w, "bassa")
}
