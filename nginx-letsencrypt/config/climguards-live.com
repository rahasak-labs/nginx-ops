server {
    listen 80;
    listen [::]:80;
    server_name climguards.com www.climguards.com;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/html;
    }
}

server {
    listen 443 ssl;
    server_name climguards.com www.climguards.com;

    ssl_certificate /etc/letsencrypt/live/climguards.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/climguards.com/privkey.pem;

    location / {
        proxy_pass http://172.17.0.1:3000;
    }
}
