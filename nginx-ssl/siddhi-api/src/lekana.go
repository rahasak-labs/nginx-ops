package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/siddhi", apiSiddhi).Methods("GET")

	// start server
	err := http.ListenAndServe(":7655", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiSiddhi(w http.ResponseWriter, r *http.Request) {
	log.Printf("INFO: siddhi api")

	fmt.Fprintln(w, "siddhi")
}
