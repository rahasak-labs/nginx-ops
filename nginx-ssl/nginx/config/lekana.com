server {
    listen          443 ssl;
    server_name     lekana.com www.lekana.com;

    ssl_certificate     /etc/nginx/certs/server.crt;
    ssl_certificate_key /etc/nginx/certs/server.key;

    location / {
        proxy_pass  http://lekana-api:7654;
    }
}
